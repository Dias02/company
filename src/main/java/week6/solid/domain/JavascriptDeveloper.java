package com.company;

public class JavascriptDeveloper extends com.company.Worker implements com.company.FrontendDeveloper {

    public JavascriptDeveloper(String name){
        super (name);
    }

    @Override
    public void develop() {
        writeFront();
    }

    @Override
    public void work() {
        develop();
    }

    @Override
    public void writeFront() {
        System.out.println("My name is "+getName()+", I am writing Javascript code for frontend");
    }
}
