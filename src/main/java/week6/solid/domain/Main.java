package com.company;

public class Main {

    public static void main(String[] args) {
        // write your code here
        Company company = new Company();
        company.addEmployee(new JavaDeveloper("Ayan"));
        company.addEmployee(new JavaDeveloper("Rakhat"));
        company.addEmployee(new JavascriptDeveloper("Ibragim"));
        company.addEmployee(new FullStackDeveloper("Aziz"));

        company.startWork();
    }
}