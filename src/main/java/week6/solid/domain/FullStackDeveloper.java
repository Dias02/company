package com.company;

public class FullStackDeveloper extends com.company.Worker implements com.company.FrontendDeveloper, com.company.BackendDeveloper {

    public FullStackDeveloper(String name){
        super(name);
    }

    @Override
    public void writeBack() {
        System.out.println("I am writing backend in c++");
    }

    @Override
    public void develop() {
        System.out.println("My name is "+getName());
        writeFront();
        writeBack();
    }

    @Override
    public void work() {
        develop();
    }

    @Override
    public void writeFront() {
        System.out.println("I am writing frontend in CSS");
    }
}
